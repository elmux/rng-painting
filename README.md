# RNG Painting

A silly little app that creates "art" out of pseudorandomness.

[Check it out](https://elmux.gitlab.io/rng-painting/)

Controls:

- Enter: Create a new piece
- Space: Pause the generation
- Left arrow: Slow down the generation
- Right arrow: Speed up the generation
- Up arrow: Increase the number of starting points
- Down arrow: Decrease the number of starting points
