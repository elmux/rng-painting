const minSize =
  window.innerWidth < window.innerHeight
    ? window.innerWidth
    : window.innerHeight;

const canvasSize = minSize < 800 ? minSize - 10 : 800;
const canvas = document.getElementById("grid");
canvas.width = canvasSize;
canvas.height = canvasSize;
const ctx = canvas.getContext("2d");
const rows = 128;
const cols = 128;
const prob = 0.25;
const fade = 16;
const width = canvasSize / cols;
const cells = [];
let interval;

let initAmount = Math.floor(Math.random() * 16) + 1;
let sleepAmount = 100;
let paused = false;

const clearCells = () => {
  for (let x = 0; x < cols; x++) {
    cells[x] = [];
    for (let y = 0; y < rows; y++) {
      cells[x][y] = { state: 0, color: "#000000" };
    }
  }
};

const drawCells = () => {
  for (let y = 0; y < rows; y++) {
    for (let x = 0; x < cols; x++) {
      ctx.fillStyle = cells[x][y].color;
      ctx.fillRect(x * width, y * width, width, width);
    }
  }
};

function activate(x, y, hex) {
  if (x < 0 || x >= cols || y < 0 || y >= rows) return;
  if (cells[x][y].state === 1) return;
  cells[x][y].state = 1;

  let r = parseInt(hex.substring(1, 3), 16);
  let g = parseInt(hex.substring(3, 5), 16);
  let b = parseInt(hex.substring(5, 7), 16);

  r += Math.floor(Math.random() * (fade * 2 + 1)) - fade;
  g += Math.floor(Math.random() * (fade * 2 + 1)) - fade;
  b += Math.floor(Math.random() * (fade * 2 + 1)) - fade;

  r = Math.min(r, 255);
  g = Math.min(g, 255);
  b = Math.min(b, 255);
  r = Math.max(r, 0);
  g = Math.max(g, 0);
  b = Math.max(b, 0);

  cells[x][y].color =
    "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

const updateCells = () => {
  const yStart = Math.random() < 0.5 ? 0 : rows - 1;
  const yEnd = yStart === 0 ? rows : -1;
  const yIncrement = yStart === 0 ? 1 : -1;
  const xStart = Math.random() < 0.5 ? 0 : cols - 1;
  const xEnd = xStart === 0 ? cols : -1;
  const xIncrement = xStart === 0 ? 1 : -1;

  for (let y = yStart; y !== yEnd; y += yIncrement) {
    for (let x = xStart; x !== xEnd; x += xIncrement) {
      if (cells[x][y].state === 1) {
        if (Math.random() < prob) activate(x - 1, y, cells[x][y].color);
        if (Math.random() < prob) activate(x + 1, y, cells[x][y].color);
        if (Math.random() < prob) activate(x, y - 1, cells[x][y].color);
        if (Math.random() < prob) activate(x, y + 1, cells[x][y].color);
      }
    }
  }
};

const randomHex = () => {
  let red = Math.floor(Math.random() * 256).toString(16);
  let green = Math.floor(Math.random() * 256).toString(16);
  let blue = Math.floor(Math.random() * 256).toString(16);
  red = red.length === 1 ? "0" + red : red;
  green = green.length === 1 ? "0" + green : green;
  blue = blue.length === 1 ? "0" + blue : blue;
  return "#" + red + green + blue;
};

canvas.addEventListener("click", function (event) {
  const x = event.clientX - canvas.offsetLeft;
  const y = event.clientY - canvas.offsetTop;
  const cellX = Math.floor(x / width);
  const cellY = Math.floor(y / width);
  cells[cellX][cellY].state = 1;
  cells[cellX][cellY].color = randomHex();
});

document.addEventListener("keydown", function (event) {
  switch (event.code) {
    case "ArrowUp":
      initAmount++;
      break;
    case "ArrowDown":
      initAmount--;
      break;
    case "ArrowLeft":
      sleepAmount *= 1.1;
      startInterval();
      break;
    case "ArrowRight":
      sleepAmount *= 0.9;
      startInterval();
      break;
    case "Enter":
      clearCells();
      initRandomCells();
      startInterval();
      break;
    case "Space":
      paused = !paused;
      break;
  }
});

const initRandomCells = () => {
  for (let i = 0; i < initAmount; i++) {
    const x = Math.floor(Math.random() * rows);
    const y = Math.floor(Math.random() * cols);
    cells[x][y].state = 1;
    cells[x][y].color = randomHex();
  }
};

const startInterval = () => {
  clearInterval(interval);
  interval = setInterval(() => {
    if (!paused) {
      updateCells();
      drawCells();
    }
  }, sleepAmount);
};

clearCells();
initRandomCells();
startInterval();
